extends Area2D

class_name ProjectileA

var direction = 0
var SPEED = 200

func _ready():
	pass

func _process(delta):
	position += Vector2(1,0).rotated(direction) * SPEED * delta

func set_direction(_direction):
	direction = _direction

func _on_body_entered(body):
	if body.name != "Player":
		queue_free()
