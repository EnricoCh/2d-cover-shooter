extends CharacterBody2D

var moving = false
var player : Player = null
var slide_direction = Vector2(1,0).normalized()
var SPEED = 1

func _physics_process(delta):
	if moving == false:
		return 
	if player == null :
		return 
	var push_vector = player.get_push_vector()
	var constrained = slide_direction.dot(push_vector)
	velocity = slide_direction * constrained * SPEED * delta
	move_and_collide(velocity)

func _on_area_2d_body_entered(body):
	if body.name != "Player":
		return
	player = body
	moving = true

func _on_area_2d_body_exited(body):
	if body.name != "Player":
		return
	moving = false

