extends CharacterBody2D

class_name Player

const SPEED = 300.0

@onready var sprite = $"PlayerSprite"
@onready var fire_tip : Node2D = $"fire_tip"
@onready var projectileAScene = preload("res://projectile_a.tscn")

func get_push_vector():
	return velocity

func _physics_process(_delta):
	var direction = Input.get_vector("west", "est", "nord", "south")
	if direction.length() != 0:
		direction = direction.normalized()
	velocity = direction * SPEED
	move_and_slide()
	look_at(get_global_mouse_position())
	if velocity.length() == 0:
		sprite.playing = false
	else:
		sprite.playing = true

func fire():
	var proj_instance : ProjectileA = projectileAScene.instantiate()
	var player_direction = get_global_transform().get_rotation()
	proj_instance.set_direction(player_direction)
	proj_instance.global_position = fire_tip.global_position
	get_parent().add_child(proj_instance)

func _input(event):
	if event.is_action_pressed("fire"):
		fire()
