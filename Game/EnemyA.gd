extends CharacterBody2D


const SPEED = 300.0

@onready var player : Player = $"../Player"
@onready var nav : NavigationAgent2D = $"NavigationAgent2D"
@onready var sprite = $"EnemyASprite"

var initial_pos = Vector2.ZERO


var target_pos : Vector2 = Vector2(0,0)

func _ready():
	initial_pos = position
	nav.velocity_computed.connect(on_velocity_computed)

func _physics_process(delta):
	
	if position.distance_to(player.global_position) < 100:
		# should interpolate
		look_at(player.position)
	if initial_pos.distance_to(player.position) < 100:
		nav.target_location = player.position
	else:
		nav.target_location = initial_pos
	if nav.is_target_reachable() and not nav.is_target_reached():
		sprite.playing = true
		var next_loc = nav.get_next_location()
		var v = (next_loc - self.global_position).normalized()
		nav.set_velocity(v)
	else:
		sprite.playing = false
	pass

func on_velocity_computed(safe_velocity: Vector2) -> void:
	self.position += safe_velocity
