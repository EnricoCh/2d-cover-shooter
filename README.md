# 2D-cover-shooter

> WIP

This is a simple 2D shooter game.  
Move the obstacles to defend from the enemy fire and shoot them back.

## TODO list

- [ ] Enemy soldiers
  - [ ] AI
  - [ ] Enemy health
- [ ] Player health
- [ ] Ammunition


## Screenshots of the game

![Screnshot 1](screenshot_2022-12-28_A.png "screenshot 1")
![Screnshot 2](screenshot_2022-12-28_B.png "screenshot 2")
![Screnshot 3](screenshot_2022-12-29_C.png "screenshot 3")
![Screnshot 4](screenshot_2022-12-29_D.png "screenshot 4")